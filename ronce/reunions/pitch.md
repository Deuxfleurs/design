Illustration Deuxfleurs
=======================

Nos réflexions sur l'illustration par Ronce, plus quelques vieilleries en queue de README.

# Notre budget

~200 euros (à préciser)

# Questions pratiques

Quel serait la license de tes illustrations ? Qu'aurions-nous le droit de faire ? de ne pas faire ? qu'est ce que tu aimerais/préferais qu'on fasse ? qu'on te prévienne avant de faire quoi ? te créditer comment ? etc. Nous ne savons pas :(

# Que faisons-nous ?

On est critique des entreprises informatiques et de leurs dérives comme le "capitalisme de surveillance".
On souhaite créer notre internet loin des logiques des grandes entreprises, en faisant au maximum nos choix, en gardant un maximum notre autonomie : objectif pas de cloud, pas de datacenter.
À la place, chaque personne vient contribuer à l'infrastructure avec quelques ordinateurs chez lui pour créer notre petit refuge sur internet.
Dans ce petit refuge, on fait ensemble de l'email, des conversations, heberger des sites webs, etc. mais à notre manière.

# À qui nous adressons nous ?

> Et après la question de "a quel imaginaire faire appel" elle est jumelée à "quels points communs culturel ont les gens à qui le site s'adresse"

Pour l'instant, on a communiqué auprès de deux cibles :

**Des personnes politisées** Des personnes qui en ont marre de tout le baratin autour du numérique mais qui ont besoin d'internet comme moyen d'expression ou juste d'une adresse email car c'est nécessaire pour eux. Ces gens là sont déjà plus ou moins au fait des organisation alternatives comme les AMAP, les initiatives de la société civiles, etc. Des gens qui sont capables de comprendre le mode associatif et qui accepteront que non, c'est pas un copy cata de Google qui deviendrait d'un coup éthique et cool. C'est différent et ouai il y a des trucs qui vont être moins bien.

**Des libristes** C'est à dire les informaticiens politisés, en faveur du logiciel libre. Eux non plus ne sont pas nécessairement convaincus de notre approche collective : souvent ils pronent un gestion indivualiste (YUNOHOST) ou par des structures très institutionalisées. Quand bien même l'approche collective est retenue, nos choix techniques les laissent perplexes : il leur semble souvent plus judicieux de centraliser la technologie au sein d'un datacenter.

À mon sens, il faut axer notre communication sur les personnes politisées et laisser de côté les libristes : de toute manière ils peuvent faire leur truc dans leur coin.

# Quel message souhaitons-nous transmettre ?

> Et une série de terme sur les emotions que vous voulez dégager (confiance, classe, rustique, confortable, impressionant, etc, voilà des exemples dont certains qui vont probablement pas vous convenir)

Du coup je partirais bien du concept du "refuge", donc :
  - rustique, sobre : l'humain est la finalité, la technique reste un outil
  - confortable mais dans le sens cosy, on vient se réchauffer autour du feu : on prend les décisions ensembles

Un truc que j'aimerais bien intégrer c'est de rendre visible les infrastructures : les emails, le chat, les sites web ce sont des ordinateurs comme le votre qui sont connectés à un réseau 24/7. Arretons de les cacher dans des usines et assumons qu'on fait ça pour ensuite choisir collectivement comment on gère ça. J'ai aucune idée de comment faire ça mais c'est un peu comme si chacun venait avec son petit bagage dans le refuge et on met en commun : bagage materiel ou connaissance d'ailleurs. En gros pour le matériel qu'on met en commun c'est cette page : https://deuxfleurs.fr/Technique/Infra/

# Que souhaitons-nous ?

> Ouais bah c'est sûr que c'est cool si vous avez une idée de comment vous comptez l'utiliser (pour avoir de mon côté une idée de la compo et de la taille de visionnage)

Nous souhaitons une ou plusieurs illustrations pour intégrer à notre page web, particulièrement la page d'accueil, qui sera complètement refondue, et à des supports de communication.
Nous avons pensé à des petites mascottes illustrant ce que l'on veut faire et qui pourraient se glisser partout (voir "références graphiques").
Cependant, rien n'est arrêté et nous faisons confiance à l'expertise de notre illustrateurice :)

Évidemment, nous reconnaissons qu'une charte graphique, qu'un logo et qu'une personne en charge de la communication globale serait un grand plus. 
Mais nous pensons que nous n'avons pas les moyens de soutenir un tel travail à sa juste valeur.
Nous attachons une grande importance à rémunérer correctement et professionnellement le travail de notre illustrateurice :)

# Des références graphiques

> Le côté "fait maison" ça renvoie instinctivement à un contraste avec des trucs ultra clean labellisés "corpo" dans notre culture visuelle. Et un aspect confiance plus forte dans les aspects style vie privée et tout. Par contre, un truc trop fourni et peu ergonomique, loin des habitudes de navigation, cest un blocage aussi. Du coup un truc clair mais ambiance "de bric et de broc" ça colle assez au côté AMAP tout en évitant de faire fuire par la complexité (si on veut du complexe incomprehsible dans l'informatique, y a déjà LinkindeIn, Facebook, la suite Adobe, etc huhuhu)

## Design végétal

* http://www.studiopaysan.net/
* http://oiadeco.com/
* Art Nouveau Font
	* [Futuracha](https://www.webdesignerdepot.com/2013/02/futuristic-typeface-echoes-art-nouveau-designs/)
	* [Decoracha](https://decoracha.holy.gd/)
	* [A cool selecta](https://thedesignest.net/art-nouveau-fonts/)
	* [Another selecta](https://hipfonts.com/art-nouveau-fonts/)


## GeoCities

![Geocities](images/geocities.jpg)

  * [Heroic fantasy](https://www.theguardian.com/books/gallery/2017/nov/22/the-art-of-terry-pratchetts-discworld-in-pictures) 
  * Cyberpunk ! [Ghost in the Shell](https://www.youtube.com/watch?v=m1bCAxHExWI), Matrix, ...
  * [NeoCities](https://neocities.org/browse) contient plein de designs bizzares/trop cool ([Big Gulp Supreme](https://biggulpsupreme.neocities.org/))

## Docker

![Docker Laurel](images/docker.jpg)

Docker est une entreprise qui a lancé un logiciel à destination des informaticiens dont les concepts étaient un peu novateurs.
Pour expliquer son fonctionnement, ils ont un personnage principal Moby la baleine et plein d'autres représentants les composants annexes.
Ci-dessus, des [dessins de Laurel](https://bloglaurel.com/post/illustrations-for-docker./36).

## Contributopia

![Contributopia](images/contributopia.jpg)

Framasoft a décidé de lancer un projet sur 3 ans pour inspirer des changements profonds dans l'informatique nommé [Contributopia](https://contributopia.org/fr/).
Ils se sont beaucoup basés sur des illustrations sur le thème de l'aventure et de l'utopie 

## Logiciels Fédérés

Ici, notre "refuge" correspondrait à une des planètes présentées : on a notre petite planète.
Là où ça devient compliqué, c'est que notre planète, on la gère collectivement, elle est composée de plusieurs ordinateurs appartenant à des personnes différentes mais on agit selon des règles communes.
Notre refuge/planète n'est pas complètement isolé, comme le montre ces images on peut "s'interconnecter" avec le reste du monde :)
This is la fédération !

### Peertube

![Peertube](images/peertube.jpg)

[PeerTube](https://joinpeertube.org/)

### Mastodon

![Mastodon](images/mastodon.jpg)
![Mastodon2](images/mastodon2.jpg)

[Mastodon](https://joinmastodon.org/)

### Mobilizon

![](images/mobilizon.jpg)

[Roman Photo lancement de Mobilizon](https://framablog.org/2020/10/27/roman-photo-visite-guidee-de-mobilizon/)

### Spritely

![Spritely](images/spritely.jpg)

[Spritely](https://spritelyproject.org/)


## Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires (CHATONS)

Eux, ce sont les petits refuges/planètes concurrentes.
En fait CHATONS c'est un label qui est donné à plein d'associations comme la notre.
Pour info, nous n'avons pas encore le label parce que c'est compliqué !
Ça nous empêche pas de regarder comment ils communiquent et de communiquer avec eux, pas besoin du label pour être une petite planète.

### Domaine Public

![](images/domainepublic.jpg)

[Domaine Public](https://www.domainepublic.net/)

### Duchesse.chat (n'existe plus)

![](images/duchesse.png)

[Ancien CHATON nantais](https://framapiaf.org/@duchesse)

### Indie Hosters

![](images/indie.png)

### NoMagic

![](images/nomagic.jpg)

### Le Filament

![](images/filament.png)

[Le Filament](https://le-filament.com/) : j'adhère pas forcément à leur charte graphique mais ils ont l'air cool x)

### Tila

![](images/tila.png)

### Zaclys

![Zaclys](images/zaclys.png)

[Zaclys](https://www.zaclys.com/) je ne suis pas fan pour le coup

<<<<<<< HEAD
* [NeoCities](https://neocities.org/browse) contient plein de designs bizzares/trop cool ([Big Gulp Supreme](https://biggulpsupreme.neocities.org/))
* Graphistes libristes : [Marie & Julien](https://mariejulien.com/)
=======
>>>>>>> 5e9b6111ee012d3f659cda71afceffbd5142c4c2
