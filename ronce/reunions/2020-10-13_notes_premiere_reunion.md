Pad ici : https://p.adnab.me/pad/#/3/pad/edit/457d513c9ff8ae5bdffd88aa87ec5c79/

Budget : 215€ (avec rallonge de 100€ possible mais à discuter)

Que dit Rose sur ce qu'on peut faire avec ce budget et ses œuvres ?
On en discute d'abord, et Rose adaptera en fonction pour y passer plus ou moins de temps (variables d'ajustement : décores etc).
Vaut mieux voir trop gros et tailler dans le gras.

Licence d'utilisation / cession de droit d'auteur : Rose nous cèdera la licence d'utilisation, mais pas le droit patrimonial (cocorico)
"droits d'exploitation" : on peut céder les droits d'exploitation pour une durée, et pour une audience (sur le web, l'audience c'est "le monde"). Le support compte aussi, par exemple des stickers réutilisant les illustrations peuvent mener à un nouveau contrat.

Rose propose qu'on parte sur un truc relativement court, limité au site internet (et sur d'autres supports).
Si on voulait décliner par exemple une mascotte sous plusieurs formats, il faudrait aussi en rediscuter.
On aurait la propriété intellectuelle de la mascotte, ce qui nous permettrait de la redessiner.
Modifier les images brutes est hors de question pour Rose. Elle préfère nous faire directement des versions découpées de son image (si on veut extraire un personnage de l'illustration).
Pour les stickers, on peut décider dès le début qu'on prend le droit d'imprimer des stickers pour les filer. Pour les commercialiser, c'est une autre histoire (il faudrait par exemple donner un pourcentage de la vente à Rose). Dans le deux cas, il faut que ça soit contractualisé.

Creative Commons : ne s'applique pas, car c'est une cession de droit à durée illimitée.

Comment s'organisent nos échanges ?

Rose propose habituellement trois croquis différents. On en choisit un. Rose montre régulièrement son avancement, propose des palettes de couleur (toxique en bas, jolie matinée, nuit). Avant que ça soit complètement fini et léché, Rose envoie une version, on valide, et elle fait la version finale.

Quentin veut faire aveuglément confiance à Rose (il est si mignon T_T), il trouve pas utile de faire 3 croquis. Mais ça fait partie du process de recherche de Rose.

À ce prix, Rose fait habituellement pas une journée de travail, donc on va s'en tenir à une illustration (et peut-être un personnage en fond transparent) pour le moment, et c'est après qu'on verra à décliner par exemple une mascotte, des logos.

Qu'est ce qu'on fait à Deuxfleurs ?

Critique des entreprises informatiques. On faisait nos petits trucs dans notre coin, et on s'est dit qu'on allait plutôt collaborer pour proposer des services ensemble, entre nous et pour l'extérieur.

On fait des choix qui peuvent paraître bizarres : on aime pas trop les data-centres, on veut pas être sur Facebook.
Les raisons sont souvent politiques : surveillance, subir les choix des gérants des plate-formes.

En résumé : en utilisant les services de Google, Facebook, etc. on subit leurs choix, on peut critiquer certaines dérives mais on arrivera à rien dans cette posture. On doit créer nos propre services internet.

Adrien dit qu'on met en commun nos infrastructures : c'est dur à maintenir, tout le monde sait pas/a pas envie de gérer. Du coup, on partage la charge de travaille. Alex gère le pad, Adrien le git, Quentin et Adrien ont chacun une instance Matrix mais c'est pas grave.


Si tu tannes des gens qui n'ont pas envie de se libérer de la Silicon Valley, tu te retrouves face à un mur de critique et mauvaise foi. On préfère viser des gens qui sont déjà politisés, ou au moins qui ont envie de se libérer de la Silicon Valley. On voudrait leur permettre de fièrement exhiber à leurs proches un service libre qui marche, qui les aidera à convaincre.

Visuellement, on veut quoi ?

Adrien : des cabanes dans les arbres reliées par des ficelles et des pots de yaourt.

Quentin : Deuxfleurs, c'est une brique de l'Internet décentralisé => un seul arbre. Plusieurs plate-formes dans le même arbre. Les autres arbres seraient d'autres acteurs du libre (Tedomum, Framasoft), ou même Gmail/Facebook. Les arbres sont reliés les uns aux autres, quels qu'ils soient.

Louison : très attaché à l'idée de "refuge". On est attaqués par les gros de l'Internet mondial, les chatons donnent un espace de confiance avec des gens de confiance, on traite avec des humains concernés. Quels animaux font des refuges ? Les fourmis (creusent des immenses galeries connectées sous terre), les castors, les hirondelles, les blaireaux (terriers super grands, des villes de blaireaux ont plusieurs siècles !). Certains animaux trouvent des refuges dans les arbres. Les écureuils sont dans des environnements très touffus/denses, et leur trimballage de graines contribue à faire perdurer les forêts.

Biodiversité de l'Internet : l'uniformisation d'une part, la révolution verte de notre côté.
Villes champignons, spores, paraistes, algues, maladie (la Silicon Valley).
Côté survie : on est plein de petits, qui peuvent s'entraider. Le pouvoir de l'amitié des mangas <3

Mascotte ? La plupart des langages de prog. ont leur mascotte, c'est facile et pratique pour avoir un symbole bien reconnaissable. Si on devait participer à un festival, ça nous donnerait un bon signe de reconnaissance. Présence graphique.
En creusant encore, on pourrait vouloir une vraie identité visuelle, mais c'est pas le plus urgent.

Terry Pratchett ? Quentin est pas un pro du droit, mais il serait triste qu'on se fasse strike notre nom pour cause de droit d'auteur. Ou prendre un procès. Ce ferait bien chier.
Il pourrait y avoir un orang-outan avec une bibliothèque dans sa cabane : référence subtile pas pareil que de réutiliser les personnages.

Pour coder en ook! https://esolangs.org/wiki/Ook!




