## Framework HTML

__OUTDATED__

### Pourquoi ?

Un framework HTML, c'est un include CSS, et parfois du JS. Ca va de Bootstrap/Foundation, qui sont des framworks immenses (avec fonction « carrousel d'images » et une foule de trucs dont on se cogne) à Skeleton/Mini, qui essayent d'être le plus simple possible tout en apportant les fonctionnalités *vitales* à un site web moderne : 

* **responsive design** : les media queries and co, qui permettent de charger du contenu différent par taille de viewport, c'est du pur HTML5/CSS et c'est chiant à écrire à la main).

	Le design par grille de 12 colonnes aide énormément à concevoir un site en pensant *mobile-first* : "Si cette div est vue sur mobile, faites qu'elle prenne toute la largeur. Si un écran PC large, faites qu'elle ne prenne que 4 colonnes sur 12."

	Je constate que certains frameworks vraiment minimalistes ne permettent pas de définir un nombre de colonnes par div en fonction de la taille du viewport. C'est soit le système de colonnes, soit toutes les div font 12 sur petit viewport. Ça suffira ou pas ?

* **normalisation**: chaque navigateur a sa propre idée de ce que devrait être la taille du corps de texte. Il faut redéfinir tous les défauts pour s'assurer un rendu homogène sur tout navigateur. C'est le taff de [Normalize.css](https://necolas.github.io/normalize.css/), inclut dans la plupart des frameworks HTML.

* **sucre** : Si la box modale info/warning/error peut être déjà définie clairement une bonne fois pour toutes, on ne sera pas tentés d'en faire une implem par box au lieu de fouiller le CSS. Ce genre de trucs, c'est pas vital et faut pas que ça *bloat*.

Non-goals :

* **LESS/SCSS** : CSS avec des variables et des includes, dans l'idée du *Don't Repeat Yourself*. Personnellement ça me saoule d'avoir à compiler du CSS avant de display, ça m'intéresse pas.

* **Node** : Plus généralement, tout framework HTML qui demande Node, ça me gave. On en aura assez à faire avec le templating Jinja ou autre.

### Qui sont les candidats ?

Sources : une recherche web "small html responsive framework" et [Awesome CSS](https://github.com/troxler/awesome-css-frameworks)

* [mini.css](https://minicss.org/) (dépôt archivé, flexible reponsive grid, CSS variables, flexbox)

	Essayé. Je voudrais un menu header à onglets sur PC qui se transforme en menu « [drawer](https://minicss.org/docs#drawer) » sur le côté pour mobile.
	Il y a des briques pour faire à peu près ça, mais la complexité de la doc me décourage. Je préfère essayer quelque chose qui ait moins d'ambitions mais soit plus facile à utiliser. Go Chota.

* [Skeleton](http://getskeleton.com/) (12 cols grid, but not responsive: either follow the defined number of columns, or full-size for small viewports)
* [Pure](https://purecss.io/) (flexible responsive grid, with shitty class names)
* **[Chota](https://jenil.github.io/chota/)** (flexible reponsive grid, CSS variables means recent browser versions only)
* [Turret.css](https://turretcss.com/) (orienté accessibilité, pas de grille ?)

### Décision

On part sur [mini.css](https://minicss.org/), parce qu'il les features que j'aime, est pas verbeux, bien minifié, et est supporté par des navigateurs assez anciens.


