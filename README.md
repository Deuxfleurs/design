# Laboratoire du futur site web de Deuxfleurs 


## Organisation du dépôt

Ce dépôt nous suit depuis un bout, et c'est donc un petit merdier plein de trésors inespérés (hum).

* `maquette/` contient des exports de notre maquette, créée sur [Penpot](https://design.penpot.app) (demandez-nous les accès si vous voulez).

* `ronce/` contient tout ce qui concerne Ronce, la graphiste qui nous a dessiné l'image dans les arbres. 

	Toutes les versions de `illustration-ronce` sont sous licence, ne l'utilisez pas sans autorisation. Nous avons l'autorisation de l'utiliser pour le site de Deuxfleurs.

	`feuilles_brush.abr` est une *brush* de l'illustration, qu'elle nous a filé au cas où on veut du bitmap assorti sur le site.

* `lab/` contient des mini-projets sous forme de pages web, qui sont des idées graphiques qu'on pourrait inclure au site final.

	`top-nav.html` contient une bonne partie du HTML qu'on attendrait dans la version finale. Hésitez pas à copier-coller.

* `assets/` contient les assets (css, fonts & images) qui sont utilisés dans `lab/`.

* `notes/` contient des notes au format texte. N'hésitez pas à y ajouter vos recherches.

* `inspirations/` contient des assets graphiques qui nous ont inspiré pendant nos recherches graphiques.

## Notes

### Objectifs techniques

* Responsive mobile-first design.
* [Media queries CSS](https://www.w3schools.com/css/css_rwd_mediaqueries.asp) : Des images envoyées adaptées à la taille des viewports.
* Pas besoin de Javascript : Le CSS3 ça déchire, on peut faire des infobulles, des animations, de la parallax. (Navigateurs compatibles depuis 7 ans ou plus.)

### Idées graphiques (`lab/`)

* **Branches et canopée** : Des bouts de branches et de canopée qui défilent lors du scroll, contenant des éléments de texte ou autre contenu.

	* `ronce/feuille brush.abr` contient la brosse Photoshop utilisée par Ronce pour les feuilles d'arbre au premier plan. On pourrait l'utiliser pour faire des bordures de fond.

* **Parallax** : Exploiter l'illustration au maximum, en la laissant fixée en fond de page tandis que les éléments graphiques défilent (parallax).
* S'il faut un menu : barre d'onglets sur PC, menu « sandwich » qui révèle une colonne d'onglets sur smartphone.

* **Top Nav** : Comment il se comporte ce menu, avec l'image pleine page quand on débarque sur le site ? Eh bah on sait pas, c'est moche dans tous les cas :D

* Couleurs : Ronce a proposé quelques couleurs dans `assets/images/gimp/branches-et-canopee`.

	> un violet du fond en rab, soit pour le fameux monde d'en bas, soit pour un peu de douceur moins saturée.

* Elements `<code>`:

	* Police [`Fira Code`](https://github.com/tonsky/FiraCode) (ou autre police stylée avec des ligatures) ?

		Il semblerait qu'Element utilise du [Fira Sans](https://fonts.google.com/specimen/Fira+Sans)... Hm.

	* [Bordures « dessinées à la main »](https://stackoverflow.com/a/6402175) ?

* Des champs de password avec des grosses puces funky. 

	Inspiration visuelle de Ghost In The Shell (Stand Alone Complex) S01E08 (cf. `inspirations/images/ghost-in-the-shell_sac_s01e08`). 

	[Guide pour le faire avec CSS](https://medium.com/@wowmotty/replace-the-input-password-bullet-1cd4ee34e0aa).

### Inspirations & Ressources

* [MDN - Création de tooltips et remarques sur l'accessibilité du contenu](https://developer.mozilla.org/en-US/docs/Web/CSS/::after#tooltips)

	On peut faire tout ce qu'il faut avec CSS3 !

	Néanmoins, *rendre un site accessible sans outils a l'air compliqué*.

* [CSS Border Effects](https://speckyboy.com/css-border-effects/) : Notamment 'Cool Clippings', 'Hand Drawn' (et 'Attention Getter' for fun)

* [border-image sur css-tricks](https://css-tricks.com/almanac/properties/b/border-image/) notamment « More Demos ».

* [W3C Semantic HTML](https://www.w3schools.com/html/html5_semantic_elements.asp)

	Utiliser des bannières qui donnent du sens au contenu.

* [Semantic CSS](https://adamwathan.me/css-utility-classes-and-separation-of-concerns/)

	Le fait de ne pas donner d'éléments de graphisme dans notre markup HTML, mais seulement dans le CSS. Par exemple, mettre une classe "text-center" à un &lt;p&gt; est considéré une mauvaise pratique. 

* [Navbar responsive pur CSS](https://codepen.io/jo_Geek/pen/xgbaEr)

* [Pollen](https://www.pollen.style/modules/typography)
* [10 CSS Resources that you should bookmark ](https://dev.to/j471n/10-css-resources-that-you-should-bookmark-46l6)