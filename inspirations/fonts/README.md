**Script**:

* [Al Fresco](https://fonts.adobe.com/fonts/al-fresco)
* [Futuracha](https://www.behance.net/gallery/5786587/Futuracha-the-font-free)

**Code**:

* [Fira Code](https://github.com/tonsky/FiraCode)

**Sans serif**:

* [Fira Sans](https://fonts.google.com/specimen/Fira+Sans)